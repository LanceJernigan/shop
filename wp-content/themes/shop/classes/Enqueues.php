<?php

namespace Pyxl\Theme;

// Un-required, but provides reference
use Pyxl\Theme\Utils;

class Enqueues
{
    public static function init()
    {
        $class = new self;
        add_action('wp_enqueue_scripts', [$class, 'register'], 25);
        add_action('get_footer', [$class, 'footer'], 50);
    }

    public function register()
    {
        // JavaScript
        $js_app = 'dist/scripts/app.js';
        wp_register_script('app', Utils::fileEnvCheck($js_app), [], Utils::versionCacheBuster($js_app), true);
        wp_localize_script(
            'app',
            'app',
            [
                'urls'    => [
                    'root'  => home_url(),
                    'ajax'  => admin_url('admin-ajax.php'),
                    'theme' => URI,
                ],
                'post_id' => get_the_ID(),
            ]
        );

        // CSS
        $css_app = 'dist/styles/app.css';
        wp_register_style('app', Utils::fileEnvCheck($css_app), [], Utils::versionCacheBuster($css_app), 'all');
    }

    public function head()
    {
        wp_enqueue_style('app');
    }

    public function footer()
    {
        wp_enqueue_script('app');
    }
}

<?php
do_action('pyxl_before_head');
?>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php
            wp_head();
        ?>
    </head>
<?php
do_action('pyxl_after_head');

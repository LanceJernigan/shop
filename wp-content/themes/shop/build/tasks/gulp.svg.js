/**
 * @link https://css-tricks.com/svg-symbol-good-choice-icons/
 */

const path = require('path');

module.exports = (gulp, pkgs, cfg) => {

    const fontAwesomeDir = path.dirname(require.resolve('font-awesome-5/package'));
    const fontAwesomeDirRawSVG = path.join(fontAwesomeDir, 'raw-svg');
    const fontAwesomeSVGs = cfg.svgSprite.fontAwesomeRawSVGs.map(item => {
        let icon = path.resolve(`${fontAwesomeDirRawSVG}/${item}.svg`);
        if (!icon) {
            return null
        }
        return icon;
    });

    const svgThemePattern = `${cfg.dir.src}/svg/**/*.svg`;
    const svgThemePaths = pkgs.globule.find(svgThemePattern);

    let svgFiles = [].concat.apply(fontAwesomeSVGs, svgThemePaths);

    return () => {
        gulp.src(svgFiles)
            .pipe(pkgs.svgo())
            .pipe(pkgs.svgSymbols({
                id: 'icon-%f',
                class: '.icon-%f',
                templates: ['default-svg'],
                warn: false,
                svgAttrs: {
                    'class': 'svg-icon-lib',
                    'aria-hidden': 'true',
                    'style': 'position:absolute;width:0;height:0;overflow:hidden;',
                    'data-enabled': 'true'
                },
            }))
            .pipe(pkgs.rename({
                basename: 'sprites',
            }))
            .pipe(gulp.dest(`${cfg.dir.dist}/svg/`));
      gulp.src(svgFiles)
        .pipe(pkgs.svgMin())
        .pipe(gulp.dest(`${cfg.dir.dist}/svg/`))
    };
};
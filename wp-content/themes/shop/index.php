<html <?php language_attributes(); ?>>

  <?php get_header(); ?>

  <body <?php body_class() ?>>

    <div id="app"></div>

    <?php get_footer(); ?>

  </body>

</html>
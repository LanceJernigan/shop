<?php

namespace Pyxl\RENAME\Controllers\Layouts;

class Hero
{
    public static function init()
    {
        $class = new self;
        add_filter('layouts/modules/hero', [$class, 'filter']);
    }

    public function filter($context)
    {
        return $context;
    }
}
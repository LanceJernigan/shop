<?php
/**
 * Plugin Name: (custom) Pyxl Starter Plugin
 * Description:
 * Version: 0.1
 * Author: Pyxl Inc.
 * Author URI: https://pyxl.com
 * License: GPLv3+
 */

namespace Pyxl\RENAME;

use Pyxl\Layouts;

defined('WPINC') || die;

define(__NAMESPACE__.'\PATH', plugin_dir_path(__FILE__));
define(__NAMESPACE__.'\URI', plugin_dir_url(__FILE__));

require_once 'lib/autoload.php';

add_action('plugins_loaded', function () {

    /**
     * Models
     */
    // Post Types
    Models\PostTypes\Sample::init();

    // Modules
    if (!class_exists(Layouts\Register::class)) {
        return;
    }
    Layouts\Register::init(
        [
            'name'    => 'Modules',
            'slug'    => 'modules',
            'path'    => PATH,
            'layouts' => array_merge([
                new Models\Layouts\Hero(),
            ], []),
        ]
    );

    /**
     * Controllers
     */
    // Post Types
    Controllers\PostTypes\Page::init();
    Controllers\PostTypes\Post::init();

});

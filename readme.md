# Local Setup 

## IMPORTANT: 
__The `setup.sh` file should be run only once and should be automated from the initial `composer install` process. If `setup.sh` ends up in the repo that is outside of `wpbase`, it should be removed (which should happend automatically if everything goes accoring to plan).__


## Intro
Having all the core WordPress files in your root can feel pretty unwieldy. However, many managed hosting solutions (like __WP Engine__) depend on the default structure for predictable maintenance.

This setup is trying to have the best of both worlds. Clean local environments, but when deployed it should seamlessly merge by sending the `wp-content` folder over (and little else if anything).

Please also note the custom `pyxl-mu` __mu-plugin__ (Must Use Plugin), this has a number of preferences for better WordPress practices. You may want to remove it, modify it, etc. Don't be shy to modify it directly. 

## Requirements
* PHP 7.0+ (will probably work with 5.6, but don't depend on it)
* Composer
* WP CLI

## Project Details
* __Local:__ `example.test`
* __Staging:__ `example.staging.wpengine.com`
* __Production:__ `example.wpengine.com`

## Getting started
* Rename `.env.example` to `.env`
* Rename `index.php.example` to `index.php`
* Rename `wp-config.php.example` to `wp-config.php`
    * This is using a very simple parser to get variables from `.env`, feel free to swap to something like `vlucas/phpdotenv` if need be.  Either way, it shouldn't be committed to the repo, it's just for your local install.
* __IMPORTANT:__ 
    * update variables in `.env`
    * Make sure the email you use for the admin account is an `@pyxl.com` address. The core plugin hides many
    WordPress menus and regions from any user without a `@pyxl.com` address.
* Run `composer install`
    * On first install will run `setup.sh`
        * Clones our starter theme
            * Renames the plugin based on the `dir_theme` variable in `.env`
        * Clones our starter core plugin
            * Renames the plugin based on the `dir_core_plugin` variable in `.env`
        * Updates `.gitignore` exceptions 
        * Removes `.git` from this repo, theme, and plugin
        * Removes `setup.sh` file

## TODO
* Evaluate any further changes to `pyxl-mu` 